# ANT Custom Translations

This module provides the ability to add custom translations from a spreadsheet.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/auto_node_translate_custom).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/auto_node_translate_custom).

## Table of contents

- Requirements
- Installation
- Configuration
- Maintainers

## Requirements

This module requires the parent module "Auto Node Translate".

## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).

## Configuration

Configure custom translations at (/admin/config/system/custom-translations)

## Maintainers

- Paulo Calado - [kallado](https://www.drupal.org/u/kallado)
- João Mauricio - [jmauricio](https://www.drupal.org/u/jmauricio)

This project has been sponsored by:

- Visit: [Javali](https://www.javali.pt) for more information
