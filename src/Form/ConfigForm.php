<?php

namespace Drupal\auto_node_translate_custom\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\File\FileSystemInterface;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use Drupal\file\Entity\File;
use PhpOffice\PhpSpreadsheet\IOFactory;

/**
 * Configure Auto Node Translate Custom settings for this site.
 */
class ConfigForm extends ConfigFormBase {

  /**
   * The language manager service.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;


  /**
   * The file system.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected $fileSystem;

  /**
   * Constructs a \Drupal\auto_node_translate\Form\TranslationForm object.
   *
   * @param \Drupal\Core\Language\LanguageManagerInterface $language_manager
   *   The language manager service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\File\FileSystemInterface $file_system
   *   The file system.
   */
  public function __construct(
    LanguageManagerInterface $language_manager,
    EntityTypeManagerInterface $entity_type_manager,
    FileSystemInterface $file_system
    ) {
    $this->languageManager = $language_manager;
    $this->entityTypeManager = $entity_type_manager;
    $this->fileSystem = $file_system;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('language_manager'),
      $container->get('entity_type.manager'),
      $container->get('file_system')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'auto_node_translate_custom_config';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['auto_node_translate_custom.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('auto_node_translate_custom.settings');
    $form['sheet'] = [
      '#type' => 'managed_file',
      '#title' => $this->t('Sheet'),
      '#default_value' => $config->get('sheet') ?? NULL,
      '#upload_location' => 'public://',
      '#upload_validators' => [
        'file_validate_extensions' => ['xlsx xls ods'],
      ],
      '#required' => TRUE,
      '#description' => $this->t('Sheet with translations in xls xlsx or ods format'),
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('auto_node_translate_custom.settings');
    $sheet = $form_state->getValues()['sheet'];
    if (!empty($sheet)) {
      $file = $this->entityTypeManager->getStorage('file')->load($sheet[0]);
      if ($file) {
        $file->setPermanent();
        $file->save();
        $config->set('sheet', $form_state->getValue('sheet'))->save();
        $this->setConfig($file);
      }
    }
  }

  /**
   * Sets the configuration from the xlsx file.
   *
   * @param \Drupal\file\Entity\File $file
   *   The file object of the uploaded file.
   */
  private function setConfig(File $file) {
    $fileUri = $file->getFileUri();
    $realpath = $this->fileSystem->realpath($fileUri);
    $spreadsheet = IOFactory::load($realpath);
    $this->setCustomTranslations($spreadsheet);
    $this->setUntranslated($spreadsheet);
    $spreadsheet->disconnectWorksheets();
    unset($spreadsheet);
  }

  /**
   * Saves the untranslatable strings in config.
   *
   * @param \PhpOffice\PhpSpreadsheet\Spreadsheet $spreadsheet
   *   The spreadsheet object.
   */
  private function setUntranslated(Spreadsheet $spreadsheet) {
    if ($spreadsheet->sheetNameExists('untrans')) {
      $spreadsheet->setActiveSheetIndexByName('untrans');
      $worksheet = $spreadsheet->getActiveSheet();
      $range = $worksheet->calculateWorksheetDimension();
      $dataArray = $worksheet->rangeToArray($range, '', TRUE, FALSE, FALSE);
      $terms = [];
      foreach ($dataArray as $term) {
        $terms[] = [
          'name' => $term[0],
        ];
      }
      $config = $this->config('auto_node_translate_custom.settings');
      $config->set('terms', $terms)
        ->save();
    }
  }

  /**
   * Saves the custom translations to config.
   *
   * @param \PhpOffice\PhpSpreadsheet\Spreadsheet $spreadsheet
   *   The spreadsheet object.
   */
  private function setCustomTranslations(Spreadsheet $spreadsheet) {
    $config = $this->config('auto_node_translate_custom.settings');
    $shetsNames = $spreadsheet->getSheetNames();
    if (($key = array_search('untrans', $shetsNames)) !== FALSE) {
      unset($shetsNames[$key]);
    }
    foreach ($shetsNames as $sheetName) {
      $spreadsheet->setActiveSheetIndexByName($sheetName);
      $worksheet = $spreadsheet->getActiveSheet();
      $range = $worksheet->calculateWorksheetDimension();
      $dataArray = $worksheet->rangeToArray($range, '', TRUE, FALSE, FALSE);
      $translationData = [];
      foreach ($dataArray as $translation) {
        if (!empty($translation[0])) {
          $translationData[] = [
            'from' => $translation[0],
            'to' => $translation[1],
          ];
        }
      }
      $config->set('trans_' . $sheetName, $translationData)
        ->save();
    }
  }

}
